// Array Remove - By John Resig (MIT Licensed)
Array.prototype.remove = function(from, to) {
  var rest = this.slice((to || from) + 1 || this.length);
  this.length = from < 0 ? this.length + from : from;
  return this.push.apply(this, rest);
};

// BEGIN GAME CODE
window.stocks = [
	{
		symbol: 'GOOG',
		name: 'Google Inc.',
		price: 888,
		trend: '-',
		risk: 4
	},
	{
		symbol: 'AAPL',
		name: 'Apple Inc.',
		price: 490,
		trend: '-',
		risk: 4
	},
	{
		symbol: 'TSCO',
		name: 'Tesco Plc',
		price: 362,
		trend: '-',
		risk: 4
	},
	{
		symbol: 'EA',
		name: 'Electronic Arts Inc.',
		price: 26,
		trend: '-',
		risk: 6
	},
	{
		symbol: 'DES.L',
		name: 'Desire Petrolium PLC',
		price: 16,
		trend: '-',
		risk: 3
	},
	{
		symbol: 'TGL.L',
		name: 'Touchstone GLD',
		price: 20,
		trend: '-',
		risk: 4
	},
	{
		symbol: 'OEX.L',
		name: 'Oilex Ltd',
		price: 30,
		trend: '-',
		risk: 5
	},
	{
		symbol: 'JIIS.L',
		name: 'JPMorgan Chinese Investment Trust PLC',
		price: 16,
		trend: '-',
		risk: 3
	},
	{
		symbol: 'GDG.L',
		name: 'Green Dragon Gas Ltd',
		price: 240,
		trend: '-',
		risk: 5
	},
	{
		symbol: 'ZZZ.L',
		name: 'Snoozebox',
		price: 13,
		trend: '-',
		risk: 3
	},
	{
		symbol: 'SYR',
		name: 'Synergy Health',
		price: 1072,
		trend: '-',
		risk: 15
	},
	{
		symbol: 'SRP',
		name: 'Serco Group',
		price: 533,
		trend: '-',
		risk: 6
	},
	{
		symbol: 'JSEC',
		name: 'JSEC Stock Exchange',
		price: 0,
		trend: '',
		risk: 0
	}
];

window.map = {};

window.myStocks = [];

window.stockExHistory = [];

window.balance = 200;
window.refreshSpeed = 1000;
window.trading = false;
window.tabStocks = '';
window.totalMinutes = 2;
window.timeLimit = (1000 * 60 * totalMinutes); // 1 Second * 60 * [TIME LIMIT (MINS)]
window.gamemode = null;

window.timer = document.getElementById('timer');
window.timing = false;

window.startTotal = document.getElementById('start-total');
window.startAverage = document.getElementById('start-average');

window.currentTotal = document.getElementById('current-total');
window.currentAverage = document.getElementById('current-average');

window.prediction = document.getElementById('prediction');

window.totalGrowth = document.getElementById('total-growth');
window.totalFall = document.getElementById('total-fall');

window.tGrowth = 0;
window.tFall = 0;

window.grflHistory = [];

function init() {
	tabStocks = document.getElementById('stocks');

	for (var i = 0; i < stocks.length; i++) {
		var me = stocks[i];
		
		if (gamemode == 'Endless')
			me.risk *= 1.5;
		
		map[me.symbol] = i;
		
		var html = '<tr id="' + me.symbol + '">';
			html += '<td>' + me.name + '</td>';
			html += '<td id="' + me.symbol + '-price">' + me.price + '</td>';
			html += '<td id="' + me.symbol + '-trend"></td>';
			html += '<td><a href="javascript:buy(\'' + me.symbol + '\');">Buy</a> | <a href="javascript:sell(\'' + me.symbol + '\')">Sell</a></td>';
		html += '</tr>';
		
		myStocks[i] = {
			name: me.symbol,
			qty: 0
		};
			
		tabStocks.innerHTML += html;
	}
	
	var total = 0;
	
	for (var i = 0; i < stocks.length; i++) {
		var me = stocks[i];
		
		if (me.symbol != 'JSEC') {
			total += me.price;
		}
		
	}
	
	startTotal.innerHTML = total;
	startAverage.innerHTML = Math.floor(total / (stocks.length - 1));
	currentTotal.innerHTML = total;
	currentAverage.innerHTML = Math.floor(total / (stocks.length - 1));
}

document.getElementById('play').onclick = function()  {
	document.getElementById('play').disabled = true;
	
	var endless = document.getElementById('endless'),
		timed   = document.getElementById('timed');
	
	endless.disabled = true;
	timed.disabled = true;
	
	if (endless.checked) {
		timer.style.display = 'none';
		timing = false;
		trading = true;
		gamemode = 'Endless';
	} else {
		timing = true;
		trading = true;
		gamemode = 'Timed'
	}
	
	init();

};

function buy(stock) {
	if (!trading) return;
	
	var max = balance / stocks[map[stock]].price;
		max = Math.floor(max);

	var qty = prompt("How many would you like to buy? (Max: " + max + ")",max);
	
	var cost = stocks[map[stock]].price * qty;
	
	if (cost <= balance && qty != null && stocks[map[stock]].price != 0) {
		balance -= cost;
		stocks[map[stock]].price += 1;
		myStocks[map[stock]].qty += parseInt(qty);
	} else {
		alert("Transaction Unsuccessful");
	}
}

window.sell = function(stock, all) {
	if (!trading) return;
	if (!all) all = false;
	
	var max = myStocks[map[stock]].qty;
	
	if (!all) {
		var qty = prompt("How many would you like to sell? (Max: " + max + ")",max);
	} else {
		var qty = max;
	}
	
	var cost = stocks[map[stock]].price * qty;
	
	if (qty <= myStocks[map[stock]].qty && qty != null) {
		balance += cost;
		
		myStocks[map[stock]].qty -= parseInt(qty);
	} else {
		alert("Transaction Unsuccessful");
	}
}

window.chart = new Highcharts.Chart({
	chart: {
		renderTo: 'chart',
		type: 'line'
	},
	title: {
		text: 'JSECSE Trend'
	},
	xAxis: {
		step: 100	
	},
	yAxis: {
		text: 'Value'
	},
	series: [
		{
			name: 'JSECSE',
			data: stockExHistory
		}
	]
});

setInterval(function() {
	if (!trading) return;

	var divBalance = document.getElementById('balance');
	var total = 0;
	divBalance.innerHTML = balance;
	
	for (var i = 0; i < stocks.length; i++) {
		var me = stocks[i];
		if (me.symbol != 'JSEC') {
			var last = me.price;
			
			if (last > 0) {
				var Rand = Math.round(1 + Math.random() * 3);
				
				if (Rand == 1 || Rand == 4) {
					me.trend = 'u';
				} else if (Rand == 2) {
					me.trend = 'd';
				} else {
					me.trend = '-';
				}
				
				var trend = me.trend;
				
				var change = Math.floor(1 + Math.random() * 2);
					change = Math.round((change / 4) * me.risk);
				
				if (trend == '-') {
					var next = last;
					var Ntrend = '';
				} else if (trend == 'u') {
					var next = last + change;
					var Ntrend = 'Growing';
					tGrowth++;
				} else {
					var next = last - change;
					var Ntrend = 'Falling';
					tFall++;
				}
				
				if (next < 0) next = 0;
				
				if (next == 0) {
					document.getElementById(me.symbol).style.color = '#aaa';
				}
			} else {
				document.getElementById(me.symbol).style.color = '#aaa';
				next = 0;
				Ntrend = 'Bankrupt';
			}
			
			total += next;
		} else {
			next = total;
			Ntrend = '';
		}
		
		document.getElementById(me.symbol + '-price').innerHTML = next;
		document.getElementById(me.symbol + '-trend').innerHTML = Ntrend;
		stocks[i].price = next;
	}
	
	var myStocksList = document.getElementById('myStocks');
	myStocksList.innerHTML = '';
	
	for (var i = 0; i < myStocks.length; i++) {
		var me = myStocks[i];
		
		if (me.qty > 0) {
			myStocksList.innerHTML += '<li><strong>' + me.name + '</strong>: ' + me.qty + '</li>';
			
			document.getElementById(me.name).style.backgroundColor = '#fffbcc';
		} else {
			document.getElementById(me.name).style.backgroundColor = '#ffffff';
		}
		
	}
	
	// Work out the value of the stock exchange
	//- Add all prices together
	//- We want to allow people to buy stocks in this too
	//- Also display a live chart at the bottom
	
	var series = chart.series[0],
		shift = series.data.length > 50;
	chart.series[0].addPoint(total, true, shift);
	
	currentTotal.innerHTML = total;
	currentAverage.innerHTML = Math.floor(total / (stocks.length - 1));
	
	totalGrowth.innerHTML = tGrowth;
	totalFall.innerHTML = tFall;
	
	var cGrow = 0;
	var cFall = 0;
	

	if (parseInt(totalFall.innerHTML) > parseInt(totalGrowth.innerHTML))
		cFall++;
	else
		cGrow++;
		
	if (tFall > tGrowth)
		grflHistory.push('Fall');
	else
		grflHistory.push('Grow');
	
	if (parseInt(currentAverage.innerHTML) > parseInt(startAverage.innerHTML)) {
		cGrow++;
		
	} else {
		cFall++;
		
	}
	
	if (grflHistory.length > 5) {
		grflHistory.remove(0);
		
		for (var i = 0; i < grflHistory.length; i++) {
			if (grflHistory[i] == 'Grow') cGrow++;
			else cFall++;
		}
	}
	
	if (cGrow == cFall) {
		// Nothing really..
	} else if (cGrow > cFall) {
		prediction.innerHTML = "Grow";
	} else {
		prediction.innerHTML = "Fall";
	}
	
}, refreshSpeed);

window.minute = 0;
window.second = 0;

var decimal = totalMinutes % 1;
	decimal = Math.round(decimal / 60);
	
minute = Math.floor(totalMinutes);
second = decimal;

timer.innerHTML = minute + ':' + (second == 0 ? '00' : second > 9 ? second : '0' + second);

setInterval(function() {
	if (!trading) return;
	if (!timing) return;
	
	if (minute <= 0 && second == 1) {
		trading = false;
		
		for (var i = 0; i < myStocks.length; i++) {
			var me = myStocks[i];
			
			var divBalance = document.getElementById('balance');
			
			if (me.qty > 0) {
				var price = stocks[map[me.name]].price;
				
				balance = price * me.qty;
				me.qty = 0;
				divBalance.innerHTML = balance;
			}
		}
		
		second = 0;
		timer.innerHTML = minute + ':' + (second == 0 ? '00' : second > 9 ? second : '0' + second);
		
		alert("Time up! You made \u00A3" + balance);
		return;
	}
	
	second--;
	
	if (second <= 0) {
		minute--;
		second = 59;
	}
	
	if (second < 30 && minute == 0) {
		$("#timer").animate({
			fontSize: '20pt'
		}, 5000).css({color: 'red'});
	}
	
	timer.innerHTML = minute + ':' + (second == 0 ? '00' : second > 9 ? second : '0' + second);
}, 1000);