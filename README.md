# Virtual Stock Trader

Virtual Stock Trader is a web-based application which allows you to trade stocks on a virtual stock market which consists of various different real companies.

__Please Note__: While the companies are real, the price of the stocks are _not_ real. They are generated randomly in such a way as to create a _random_ upwards or downwards trend.

__Second Note__: While a typical stock market trades in pennies, Virtual Stock Trader values the stocks of each company in pounds.

## Game Modes
Virtual Stock Trader has two different game modes: __Timed__ and __Endless__.

### Timed
In timed mode you are racing against the clock to try and earn the most money you can in two minutes. This game mode is good if you want to play this game competitively against friends.

### Endless
In endless mode you can sit back and relax; it's time to make long term investments as you have all the time in the world to make your millions on the stock market. In this game mode the risk is upped, meaning companies are able to lose money, and go bankrupt, faster.